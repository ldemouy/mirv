# MIRV
A chatbot in Rust primarily targeted at Twitch, but also designed to be 
extensible to other platforms.  The MIRV name references the distributed nature
of the program architecture.

This program consists of two primary components:

- Connections: Programs that handle the connection to particular chat servers.
These connections are configured by arguments passed in at launch. They convert
These messages into a protocol independent message and send these over a 
Unix Domain Socket to the Controller.
- Controller: A Program that listens on the Unix Domain Socket: 
/var/run/mirv.socket, and reacts to messages, by invoking command objects. Each
command object implements the ICommand Interface and is it's own module in 
cargo.toml. 

# Commands Currently Supported:

# Planned Commands:
- ```SendJackBoxCode <target> <code>```
    - Send a Jackbox code to a specified user or appropriate users of a channel.
    On Twitch mods and subscribers as well as those who have sent the play 
    command within the past 10 minutes are sent the code. Internally using 
    PM_PRIVLEGED.

